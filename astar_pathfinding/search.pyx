# cython: profile=True

import heapq
import math

from cpython.object cimport PyObject
from .custom_functions cimport cost1, cost2, cost3, cost4
from .search_helpers cimport euclidean_distance


cdef class Cell(object):
    def __init__(self, x, y, z, h, reachable):
        """Initialize new cell.

        @param reachable is cell reachable? not a wall?
        @param x cell x coordinate
        @param y cell y coordinate
        @param g cost to move from the starting cell to this cell.
        @param h estimation of the cost to move from this cell
                 to the ending cell.
        @param f f = g + h
        """
        self.reachable = reachable
        self.x = x
        self.y = y
        self.z = z
        self.parent = None
        self.g = 0
        self.h = 0
        self.f = 0
        self.yaw = h

    def __lt__(self, other):
        return self.f < other.f

    def __eq__(self, other):
        return (self.x == other.x) and (self.y == other.y) and (self.z == other.z) and (self.yaw == other.yaw)

    def __hash__(self):
        return hash((self.x, self.y, self.z, self.yaw))

    def __repr__(self):
        return " {} -- ({},{},{},{}) {}+{}={} Parent:{}".format(self.reachable, self.x, self.y, self.z, self.yaw,
                                                                self.g, self.h, self.f, str(self.parent))

cdef class Search:
    # cdef Pos **walls
    # cdef PyCell **cells 

    # def __cinit__(self):
    #     # open list
    #     self.opened = []
    #     # grid cells
    #     self.cells = []
    #     self.grid_height = None
    #     self.grid_width = None
    #     self.grid_depth = None
    #     self.walls = None
        
    def __init__(self, variant=1):
        # visited cells list
        self.closed = set()

        # open list
        self.opened = []

        # grid cells
        self.cells = []
        self.grid_height = 20
        self.grid_width = 20
        self.grid_depth = 3
        self.walls = []

        self.variant = variant
        self.estimate = 4

        if variant == 1 or variant == 2:
            self.estimate = 10
        elif variant == 3 or variant == 4:
            self.estimate = 4

        heapq.heapify(self.opened)
        self.hlist = [8, 7, 6, 5, 4, 3, 2, 1]

    cpdef init_grid(self, width, height, walls, start, end, depth):
        """Prepare grid cells, walls.

        @param width grid's width.
        @param height grid's height.
        @param walls list of wall x,y tuples.
        @param start grid starting point x,y tuple.
        @param end grid ending point x,y tuple.
        :param yaw:
        """
        self.grid_height = height
        self.grid_width = width
        self.grid_depth = depth
        self.walls = walls

        self.cells = []

        for x in range(self.grid_width):
            for y in range(self.grid_height):
                for z in range(self.grid_depth):
                    for h in range(9):
                        if (x, y, z) in self.walls:
                            reachable = 0
                        else:
                            reachable = 1

                        self.cells.append(Cell(x, y, z, h, reachable))

        self.start = self.get_cell(start[0], start[1], start[2], start[3])
        self.end = self.get_cell(end[0], end[1], end[2], end[3])

        if len(self.hlist) > 7:
            self.hlist.remove(self.end.yaw)

    def get_heuristic(self, cell):
        """Compute the heuristic value H for a cell.

        Default heuristic function.  Distance between this cell and the ending cell multiply by 10.

        @returns heuristic value H
        """
        # return 10 * (abs(cell.x - self.end.x) + abs(cell.y - self.end.y) + abs(cell.z - self.end.z))
        return 10*math.sqrt((self.end.x - cell.x)**2 + (self.end.y - cell.y)**2 + (self.end.z - cell.z)**2)

    def get_cell(self, x, y, z, h):
        """Returns a cell from the cells list.

        @param x cell x coordinate
        @param y cell y coordinate
        @returns cell
        """
        return self.cells[x * (self.grid_width * self.grid_depth * 9) + y * (self.grid_depth * 9) + (z * 9) + h]

    cpdef get_adjacent_cells(self, cell):
        """Returns adjacent cells to a cell.

        Clockwise starting from the one on the right.

        @param cell get adjacent cells for this cell
        @returns adjacent cells list.
        """
        cells = []
        valid_adjacents = []

        if 8 > cell.yaw > 1:
            valid_adjacents.append(cell.yaw - 1)
            valid_adjacents.append(cell.yaw)
            valid_adjacents.append(cell.yaw + 1)
        elif cell.yaw == 8:
            valid_adjacents.append(7)
            valid_adjacents.append(8)
            valid_adjacents.append(1)
        else:
            valid_adjacents.append(8)
            valid_adjacents.append(1)
            valid_adjacents.append(2)

        for pos in valid_adjacents:
            if pos == 1:
                if cell.y > 0:
                    c = self.get_cell(cell.x, cell.y - 1, cell.z, 1)
                    cells.append(c)

                    if cell.z < (self.grid_depth - 1):
                        c = self.get_cell(cell.x, cell.y - 1, cell.z + 1, 1)
                        cells.append(c)

                    if cell.z > 0:
                        c = self.get_cell(cell.x, cell.y - 1, cell.z - 1, 1)
                        cells.append(c)
            elif pos == 2:
                if cell.x < self.grid_width - 1 and cell.y > 0:
                    c = self.get_cell(cell.x + 1, cell.y - 1, cell.z, 2)
                    cells.append(c)

                    if cell.z < (self.grid_depth - 1):
                        c = self.get_cell(cell.x + 1, cell.y - 1, cell.z + 1, 2)
                        cells.append(c)

                    if cell.z > 0:
                        c = self.get_cell(cell.x + 1, cell.y - 1, cell.z - 1, 2)
                        cells.append(c)
            elif pos == 3:
                if cell.x < self.grid_width - 1:
                    c = self.get_cell(cell.x + 1, cell.y, cell.z, 3)
                    cells.append(c)

                    if cell.z < (self.grid_depth - 1):
                        c = self.get_cell(cell.x + 1, cell.y, cell.z + 1, 3)
                        cells.append(c)

                    if cell.z > 0:
                        c = self.get_cell(cell.x + 1, cell.y, cell.z - 1, 3)
                        cells.append(c)
            elif pos == 4:
                if cell.x < self.grid_width - 1 and cell.y < self.grid_height - 1:
                    c = self.get_cell(cell.x + 1, cell.y + 1, cell.z, 4)
                    cells.append(c)

                    if cell.z < (self.grid_depth - 1):
                        c = self.get_cell(cell.x + 1, cell.y + 1, cell.z + 1, 4)
                        cells.append(c)

                    if cell.z > 0:
                        c = self.get_cell(cell.x + 1, cell.y + 1, cell.z - 1, 4)
                        cells.append(c)
            elif pos == 5:
                if cell.y < self.grid_height - 1:
                    c = self.get_cell(cell.x, cell.y + 1, cell.z, 5)
                    cells.append(c)

                    if cell.z < (self.grid_depth - 1):
                        c = self.get_cell(cell.x, cell.y + 1, cell.z + 1, 5)
                        cells.append(c)

                    if cell.z > 0:
                        c = self.get_cell(cell.x, cell.y + 1, cell.z - 1, 5)
                        cells.append(c)
            elif pos == 6:
                if cell.x > 0 and cell.y < self.grid_height - 1:
                    c = self.get_cell(cell.x - 1, cell.y + 1, cell.z, 6)
                    cells.append(c)

                    if cell.z < (self.grid_depth - 1):
                        c = self.get_cell(cell.x - 1, cell.y + 1, cell.z + 1, 6)
                        cells.append(c)

                    if cell.z > 0:
                        c = self.get_cell(cell.x - 1, cell.y + 1, cell.z - 1, 6)
                        cells.append(c)
            elif pos == 7:
                if cell.x > 0:
                    c = self.get_cell(cell.x - 1, cell.y, cell.z, 7)
                    cells.append(c)

                    if cell.z < (self.grid_depth - 1):
                        c = self.get_cell(cell.x - 1, cell.y, cell.z + 1, 7)
                        cells.append(c)

                    if cell.z > 0:
                        c = self.get_cell(cell.x - 1, cell.y, cell.z - 1, 7)
                        cells.append(c)
            elif pos == 8:
                if cell.x > 0 and cell.y > 0:
                    c = self.get_cell(cell.x - 1, cell.y - 1, cell.z, 8)
                    cells.append(c)

                    if cell.z < (self.grid_depth - 1):
                        c = self.get_cell(cell.x - 1, cell.y - 1, cell.z + 1, 8)
                        cells.append(c)

                    if cell.z > 0:
                        c = self.get_cell(cell.x - 1, cell.y - 1, cell.z - 1, 8)
                        cells.append(c)

        return cells

    def get_path(self):
        cell = self.end
        path = [(cell.x, cell.y, cell.z, cell.yaw)]
        while cell.parent is not self.start:
            if cell.parent is not None:
                cell = cell.parent
                path.append((cell.x, cell.y, cell.z, cell.yaw))
            if cell == self.start:
                break

        path.append((self.start.x, self.start.y, self.start.z, self.start.yaw))
        path.reverse()

        # print("Final path = %s" % str(path))

        new_path = []
        if path is not None:
            last_item = None
            for item in path:
                new_path.append((item[0], item[1], item[2], item[3]))

                if last_item is not None:
                    delta = self.angle_delta(item[3], last_item[3])
                    if delta > 1:
                        print("Found a path segment with > 45 angle movement - %d" % delta)

                last_item = item

        return path

    cpdef get_cost(self, adj, cell):
        """Find the cost of a cell adjacent to another.

        @param adj adjacent cell to current cell
        @param cell current cell being processed
        """

        if self.variant == 1:
            return cost1(adj, cell, self.walls)
        elif self.variant == 2:
            return cost2(adj, cell, self.walls)
        elif self.variant == 3:
            return cost3(adj, cell, self.walls)
        elif self.variant == 4:
            return cost4(adj, cell, self.walls)
        else:
            return cost1(adj, cell, self.walls) 

    cpdef update_cell(self, adj, cell, cost=None):
        """Update adjacent cell.

        @param adj adjacent cell to current cell
        @param cell current cell being processed
        """
        
        new_cost = 0

        if cost is not None:
            new_cost = cost
        else:
            new_cost = self.get_cost(adj, cell)
        
        adj.parent = cell
        adj.g = cell.g + cost
        adj.h = self.get_heuristic(adj)
        adj.f = adj.h + adj.g

    @staticmethod
    def angle_delta(a, b):
        val = abs(a - b)

        if val > 4:
            if val == 5:
                return 3
            elif val == 6:
                return 2
            else:
                return 1
        else:
            return val

    cpdef determine_yaw_between_cells(self, adj_x, adj_y, cell_x, cell_y):
        x1 = adj_x
        x2 = cell_x
        y1 = adj_y
        y2 = cell_y
        angle = math.degrees(math.atan2(x2 - x1, y2 - y1)) % 360

        if (0.0 <= angle and angle < 22.5) or (337.5 <= angle and angle <= 360):
            return 5
        elif 22.5 <= angle and angle < 67.5:
            return 4
        elif 67.5 <= angle and angle < 112.5:
            return 3
        elif 112.5 <= angle and angle < 157.5:
            return 2
        elif 157.5 <= angle and angle < 202.5:
            return 1
        elif 202.5 <= angle and angle < 247.5:
            return 8
        elif 247.5 <= angle and angle < 292.5:
            return 7
        elif 292.5 <= angle and angle < 337.5:
            return 6

    cpdef solve(self):
        """Solve maze, find path to ending cell.

        @returns path or None if not found.
        """
        # add starting cell to open heap queue
        heapq.heappush(self.opened, (self.start.f, self.start,))
        while len(self.opened):
            # pop cell from heap queue
            f, cell = heapq.heappop(self.opened)
            #print("Focus on f={} cell= {}".format(f, cell))
            # add cell to closed list so we don't process it twice
            self.closed.add(cell)
            #print("Closed list: {}".format(self.closed))
            # if ending cell, return found path
            if cell is self.end:
                da_path = self.get_path()
                if da_path is not None:
                    return da_path
            # get adjacent cells for cell
            adj_cells = self.get_adjacent_cells(cell)
            for adj_cell in adj_cells:
                if adj_cell.reachable == 1 and adj_cell not in self.closed:
                    new_cost = self.get_cost(adj_cell, cell) # Get path cost to this adjacent cell
                    if (adj_cell.f, adj_cell) in self.opened:
                        # if adj cell in open list, check if current path is
                        # better than the one previously found
                        # for this adj cell.
                        if adj_cell.g < new_cost:
                            # print("Updating cell... (%d, %d)" % (cell.x, cell.y))
                            self.update_cell(adj_cell, cell, cost=new_cost)
                    else:
                        self.update_cell(adj_cell, cell, cost=new_cost)
                        # add adj cell to open list
                        heapq.heappush(self.opened, (adj_cell.f, adj_cell))
                    # print("Looking at %d, %d, %d" % (adj_cell.x, adj_cell.y, adj_cell.z))
            #input("Press Enter to continue...")
        if len(self.hlist) == 0:
            return None
        else:
            new_end = (self.end.x, self.end.y, self.end.z, self.hlist.pop())
            # print("Searching for new goal: %s" % str(new_end))
            self.init_grid(self.grid_width, self.grid_height, self.walls, (self.start.x, self.start.y, self.start.z, self.start.yaw), new_end, self.grid_depth)
            return self.solve()

