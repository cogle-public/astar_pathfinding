from .search_helpers cimport euclidean_distance, determine_yaw_between_cells

DIRS = {1: (0, -1), 2: (1, -1), 3: (1, 0), 4: (1, 1), 5: (0,1), 6: (-1, 1), 7: (-1, 0), 8: (-1, -1)}
ANGLED = [2, 4, 6, 8]


# Variant 1: 10 Straight, 14 Angled, 20 for Z Change
# Variant 2: 10 Straight, +4 Angled, +10 for Z Change, +15 for wall proximity
# Variant 3: 4 Straight,  6 Angled,  +2 for Z Change
# Variant 4: 4 Straight,  6 Angled,  +2 for Z Change, +1 for wall proximity

cpdef cost1(adj, cell, walls):
    """Find the cost of a cell adjacent to another (1st variant).

    @param adj adjacent cell to current cell
    @param cell current cell being processed
    """

    cost = 10

    if determine_yaw_between_cells(cell.x, cell.y, adj.x, adj.y) in ANGLED:
        cost = 14

    if (adj.z - cell.z) != 0:
        cost = 20

    return cost

cpdef cost2(adj, cell, walls):
    """Find the cost of a cell adjacent to another (2nd variant).

    @param adj adjacent cell to current cell
    @param cell current cell being processed
    """

    heading = determine_yaw_between_cells(adj.x, adj.y, cell.x, cell.y)
    cost = 10

    if (adj.z != cell.z) != 0:
        cost += 10

    if heading in ANGLED:
        cost += 4

    # Get cost for being near walls
    cost += get_wall_proximity_cost(adj.x, adj.y, adj.z, heading, walls, 15)
    
    return cost

cpdef cost3(adj, cell, walls):
    """Find the cost of a cell adjacent to another. (3rd variant)

    @param adj adjacent cell to current cell
    @param cell current cell being processed
    """

    heading = determine_yaw_between_cells(adj.x, adj.y, cell.x, cell.y)
    cost = 4

    if heading in [2, 4, 6, 8]:
        cost = 6

    if (adj.z != cell.z) != 0:
        cost += 2
    
    return cost

cpdef cost4(adj, cell, walls):
    """Find the cost of a cell adjacent to another. (4th variant)

    @param adj adjacent cell to current cell
    @param cell current cell being processed
    """

    heading = determine_yaw_between_cells(adj.x, adj.y, cell.x, cell.y)
    cost = 4

    if heading in ANGLED:
        cost = 6

    if (adj.z != cell.z) != 0:
        cost += 2

    # Get cost for being near walls
    cost += get_wall_proximity_cost(adj.x, adj.y, adj.z, heading, walls, 1)
    
    return cost

cpdef get_wall_proximity_cost(x, y, z, h, walls, w_cost):
    """Determine cost from locally adjacent hazards (walls)

    :param x: cell position
    :param y: cell position
    :param z: cell position
    :param h: cell heading
    :return: cost determined by 2 * immediately adjacent walls
    """
    yaw_arc = {1: [7, 8, 1, 2, 3], 2: [8, 1, 2, 3, 4], 3: [1, 2, 3, 4, 5], 4: [2, 3, 4, 5, 6], 5: [3, 4, 5, 6, 7],
                6: [4, 5, 6, 7, 8], 7: [5, 6, 7, 8, 1], 8: [6, 7, 8, 1, 2]}

    wall_check_set = []
    for heading in yaw_arc[h]:
        position = DIRS[heading]
        wall_check_set.append((x + position[0], y + position[1], z))

    cost = 0

    for check in wall_check_set:
        if check in walls:
            cost += w_cost

    return cost

