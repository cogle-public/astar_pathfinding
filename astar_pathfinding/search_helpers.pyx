import math

cpdef euclidean_distance(x1, y1, x2, y2):
    return math.sqrt((x2 - x1)**2 + (y2 - y1)**2)

cpdef euclidean_distance3d(x1, y1, z1, x2, y2, z2):
    return math.sqrt((x2 - x1)**2 + (y2 - y1)**2 + (z2 - z1)**2)

cpdef determine_yaw_between_cells(adj_x, adj_y, cell_x, cell_y):
        x1 = adj_x
        x2 = cell_x
        y1 = adj_y
        y2 = cell_y
        angle = math.degrees(math.atan2(x2 - x1, y2 - y1)) % 360

        if (0.0 <= angle and angle < 22.5) or (337.5 <= angle and angle <= 360):
            return 5
        elif 22.5 <= angle and angle < 67.5:
            return 4
        elif 67.5 <= angle and angle < 112.5:
            return 3
        elif 112.5 <= angle and angle < 157.5:
            return 2
        elif 157.5 <= angle and angle < 202.5:
            return 1
        elif 202.5 <= angle and angle < 247.5:
            return 8
        elif 247.5 <= angle and angle < 292.5:
            return 7
        elif 292.5 <= angle and angle < 337.5:
            return 6