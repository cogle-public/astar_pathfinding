import pstats, cProfile

from astar_pathfinding.search cimport Search

cdef class AStar:
    def __init__(self, variant=1):
        self.search = Search(variant)

    cpdef solve(self, width, height, walls, start, end, depth):
        # TODO: Convert to c types
        self.search.init_grid(width, height, walls, start, end, depth)
        return self.search.solve()

    cpdef profile_solve(self, width, height, walls, start, end, depth):
        cProfile.runctx("self.solve({},{},{},{},{},{})".format(width, height, walls, start, end, depth), globals(), locals(), "Profile.prof")

        s = pstats.Stats("Profile.prof")
        s.strip_dirs().sort_stats("time").print_stats()