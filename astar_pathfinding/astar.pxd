from astar_pathfinding.search cimport Search

cdef class AStar:
    cdef Search search

    cpdef solve(self, width, height, walls, start, end, depth)

    cpdef profile_solve(self, width, height, walls, start, end, depth)