cdef class Cell:
    cdef public int x, y, z, yaw, reachable
    cdef public float g, h, f
    cdef public Cell parent

cdef class Search:
    cdef int grid_height, grid_width, grid_depth, variant, estimate
    cdef set closed
    cdef list opened, cells, walls, hlist
    cdef Cell start, end

    cpdef init_grid(self, width, height, walls, start, end, depth)
    cpdef get_adjacent_cells(self, cell)
    cpdef get_cost(self, adj, cell)
    cpdef update_cell(self, adj, cell, cost=*)
    cpdef determine_yaw_between_cells(self, adj_x, adj_y, cell_x, cell_y)
    cpdef solve(self)
