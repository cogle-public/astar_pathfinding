#!/bin/bash

# Performs a unit test of the local files and cleans up the built files afterwards.

activate () {
  source VENV/bin/activate
  python3 setup.py build_ext --inplace
  cythonize -i tests/local_test.pyx
  python3 -c "import tests.local_test"
  echo "Cleaning up..."
  rm tests/local_test.cpython*.so
  rm tests/local_test.c
}

activate