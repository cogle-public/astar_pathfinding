#!/bin/bash
#
# Installs a local virtual environment
#
activate () {
  source ./VENV/bin/activate
  pip3 install -r requirements.txt
}

test_environment () {
  source ./LIBENV/bin/activate
  pip3 install -r ../requirements.txt
  pip3 install git+https://gitlab.com/cogle-public/astar_pathfinding
  deactivate
}

rm -rf tests/VENV
cd tests virtualenv LIBENV
test_environment
cd ..

rm -rf VENV
virtualenv VENV
activate
