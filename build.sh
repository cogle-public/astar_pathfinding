#!/bin/bash

activate () {
  source ./VENV/bin/activate
  python3 setup.py build_ext --inplace --verbose
}

activate