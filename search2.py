import heapq
import math


class Cell(object):
    def __init__(self, x, y, z, h, reachable):
        """Initialize new cell.

        @param reachable is cell reachable? not a wall?
        @param x cell x coordinate
        @param y cell y coordinate
        @param g cost to move from the starting cell to this cell.
        @param h estimation of the cost to move from this cell
                 to the ending cell.
        @param f f = g + h
        """
        self.reachable = reachable
        self.x = x
        self.y = y
        self.z = z
        self.parent = None
        self.g = 0
        self.h = 0
        self.f = 0
        self.yaw = h

    def __lt__(self, other):
        return self.f < other.f

    def __repr__(self):
        return " {} -- ({},{},{},{}) {}+{}={} Parent:{}".format(self.reachable, self.x, self.y, self.z, self.yaw,
                                                                self.g, self.h, self.f, str(self.parent))


class AStar2(object):
    def __init__(self):
        # open list
        self.opened = []
        heapq.heapify(self.opened)
        # visited cells list
        self.closed = set()
        # grid cells
        self.cells = []
        self.grid_height = None
        self.grid_width = None
        self.grid_depth = None
        self.walls = None
        self.hlist = [8, 7, 6, 5, 4, 3, 2, 1]

    def init_grid(self, width, height, walls, start, end, depth):
        """Prepare grid cells, walls.

        @param width grid's width.
        @param height grid's height.
        @param walls list of wall x,y tuples.
        @param start grid starting point x,y tuple.
        @param end grid ending point x,y tuple.
        :param yaw:
        """
        self.grid_height = height
        self.grid_width = width
        self.grid_depth = depth
        self.walls = walls

        self.cells = []

        for x in range(self.grid_width):
            for y in range(self.grid_height):
                for z in range(self.grid_depth):
                    for h in range(9):
                        if (x, y, z) in walls:
                            reachable = False
                        else:
                            reachable = True
                        self.cells.append(Cell(x, y, z, h, reachable))

        self.start = self.get_cell(*start)
        self.end = self.get_cell(*end)

        if len(self.hlist) > 7:
            self.hlist.remove(self.end.yaw)

    def get_heuristic(self, cell):
        """Compute the heuristic value H for a cell.

        Distance between this cell and the ending cell multiply by 10.

        @returns heuristic value H
        """
        # return 12 * (abs(cell.x - self.end.x) + abs(cell.y - self.end.y) + abs(cell.z - self.end.z))
        return 20*math.sqrt((self.end.x - cell.x)**2 + (self.end.y - cell.y)**2 + (self.end.z - cell.z)**2)

    def get_cell(self, x, y, z, h):
        """Returns a cell from the cells list.

        @param x cell x coordinate
        @param y cell y coordinate
        @returns cell
        """
        return self.cells[x * (self.grid_width * self.grid_depth * 9) + y * (self.grid_depth * 9) + (z * 9) + h]

    def get_adjacent_cells(self, cell):
        """Returns adjacent cells to a cell.

        Clockwise starting from the one on the right.

        @param cell get adjacent cells for this cell
        @returns adjacent cells list.
        """
        cells = []
        valid_adjacents = []

        if 8 > cell.yaw > 1:
            valid_adjacents.append(cell.yaw - 1)
            valid_adjacents.append(cell.yaw)
            valid_adjacents.append(cell.yaw + 1)
        elif cell.yaw == 8:
            valid_adjacents.append(7)
            valid_adjacents.append(8)
            valid_adjacents.append(1)
        elif cell.yaw == 1:
            valid_adjacents.append(8)
            valid_adjacents.append(1)
            valid_adjacents.append(2)

        for pos in valid_adjacents:
            if pos == 1:
                if cell.y > 0:
                    c = self.get_cell(cell.x, cell.y - 1, cell.z, 1)
                    cells.append(c)

                    if cell.z < (self.grid_depth - 1):
                        c = self.get_cell(cell.x, cell.y - 1, cell.z + 1, 1)
                        cells.append(c)

                    if cell.z > 0:
                        c = self.get_cell(cell.x, cell.y - 1, cell.z - 1, 1)
                        cells.append(c)
            elif pos == 2:
                if cell.x < self.grid_width - 1 and cell.y > 0:
                    c = self.get_cell(cell.x + 1, cell.y - 1, cell.z, 2)
                    cells.append(c)

                    if cell.z < (self.grid_depth - 1):
                        c = self.get_cell(cell.x + 1, cell.y - 1, cell.z + 1, 2)
                        cells.append(c)

                    if cell.z > 0:
                        c = self.get_cell(cell.x + 1, cell.y - 1, cell.z - 1, 2)
                        cells.append(c)
            elif pos == 3:
                if cell.x < self.grid_width - 1:
                    c = self.get_cell(cell.x + 1, cell.y, cell.z, 3)
                    cells.append(c)

                    if cell.z < (self.grid_depth - 1):
                        c = self.get_cell(cell.x + 1, cell.y, cell.z + 1, 3)
                        cells.append(c)

                    if cell.z > 0:
                        c = self.get_cell(cell.x + 1, cell.y, cell.z - 1, 3)
                        cells.append(c)
            elif pos == 4:
                if cell.x < self.grid_width - 1 and cell.y < self.grid_height - 1:
                    c = self.get_cell(cell.x + 1, cell.y + 1, cell.z, 4)
                    cells.append(c)

                    if cell.z < (self.grid_depth - 1):
                        c = self.get_cell(cell.x + 1, cell.y + 1, cell.z + 1, 4)
                        cells.append(c)

                    if cell.z > 0:
                        c = self.get_cell(cell.x + 1, cell.y + 1, cell.z - 1, 4)
                        cells.append(c)
            elif pos == 5:
                if cell.y < self.grid_height - 1:
                    c = self.get_cell(cell.x, cell.y + 1, cell.z, 5)
                    cells.append(c)

                    if cell.z < (self.grid_depth - 1):
                        c = self.get_cell(cell.x, cell.y + 1, cell.z + 1, 5)
                        cells.append(c)

                    if cell.z > 0:
                        c = self.get_cell(cell.x, cell.y + 1, cell.z - 1, 5)
                        cells.append(c)
            elif pos == 6:
                if cell.x > 0 and cell.y < self.grid_height - 1:
                    c = self.get_cell(cell.x - 1, cell.y + 1, cell.z, 6)
                    cells.append(c)

                    if cell.z < (self.grid_depth - 1):
                        c = self.get_cell(cell.x - 1, cell.y + 1, cell.z + 1, 6)
                        cells.append(c)

                    if cell.z > 0:
                        c = self.get_cell(cell.x - 1, cell.y + 1, cell.z - 1, 6)
                        cells.append(c)
            elif pos == 7:
                if cell.x > 0:
                    c = self.get_cell(cell.x - 1, cell.y, cell.z, 7)
                    cells.append(c)

                    if cell.z < (self.grid_depth - 1):
                        c = self.get_cell(cell.x - 1, cell.y, cell.z + 1, 7)
                        cells.append(c)

                    if cell.z > 0:
                        c = self.get_cell(cell.x - 1, cell.y, cell.z - 1, 7)
                        cells.append(c)
            elif pos == 8:
                if cell.x > 0 and cell.y > 0:
                    c = self.get_cell(cell.x - 1, cell.y - 1, cell.z, 8)
                    cells.append(c)

                    if cell.z < (self.grid_depth - 1):
                        c = self.get_cell(cell.x - 1, cell.y - 1, cell.z + 1, 8)
                        cells.append(c)

                    if cell.z > 0:
                        c = self.get_cell(cell.x - 1, cell.y - 1, cell.z - 1, 8)
                        cells.append(c)

        return cells

    def get_path(self):
        cell = self.end
        path = [(cell.x, cell.y, cell.z, cell.yaw)]
        while cell.parent is not self.start:
            if cell.parent is not None:
                cell = cell.parent
                path.append((cell.x, cell.y, cell.z, cell.yaw))

        path.append((self.start.x, self.start.y, self.start.z, self.start.yaw))
        path.reverse()

        # print("Final path = %s" % str(path))

        new_path = []
        if path is not None:
            last_item = None
            for item in path:
                new_path.append((item[0], item[1], item[2], item[3]))

                if last_item is not None:
                    delta = self.angle_delta(item[3], last_item[3])
                    if delta > 1:
                        print("Found a path segment with > 45 angle movement - %d" % delta)

                last_item = item

        return path

    def get_wall_proximity_cost(self, x, y, z, h):
        """Determine cost from locally adjacent hazards (walls)

        :param x: cell position
        :param y: cell position
        :param z: cell position
        :param h: cell heading
        :return: cost determined by 2 * immediately adjacent walls
        """
        map = {1: (0, -1), 2: (1, -1), 3: (1, 0), 4: (1, 1), 5: (0,1), 6: (-1, 1), 7: (-1, 0), 8: (-1, -1)}
        yaw_arc = {1: [7, 8, 1, 2, 3], 2: [8, 1, 2, 3, 4], 3: [1, 2, 3, 4, 5], 4: [2, 3, 4, 5, 6], 5: [3, 4, 5, 6, 7],
                   6: [4, 5, 6, 7, 8], 7: [5, 6, 7, 8, 1], 8: [6, 7, 8, 1, 2]}

        wall_check_set = []
        for heading in yaw_arc[h]:
            position = map[heading]
            wall_check_set.append((x + position[0], y + position[1], z))

        cost = 0

        for check in wall_check_set:
            if check in self.walls:
                cost += 15

        return cost


    def get_cost(self, adj, cell):
        """Find the cost of a cell adjacent to another.

        @param adj adjacent cell to current cell
        @param cell current cell being processed
        """

        heading = self.determine_yaw_between_cells(adj.x, adj.y, cell.x, cell.y)
        cost = 10

        if (adj.z != cell.z) != 0:
            cost += 10

        if heading in [2, 4, 6, 8]:
            cost += 4

        # Get cost for being near walls
        cost += self.get_wall_proximity_cost(adj.x, adj.y, adj.z, heading)
        
        return cost
        

    def update_cell(self, adj, cell, cost=None):
        """Update adjacent cell.

        @param adj adjacent cell to current cell
        @param cell current cell being processed
        """
        new_cost = 0

        if cost is not None:
            new_cost = cost
        else:
            new_cost = self.get_cost(adj, cell)

        adj.parent = cell
        adj.g = cell.g + cost
        adj.h = self.get_heuristic(adj)
        adj.f = adj.h + adj.g

    @staticmethod
    def angle_delta(a, b):
        val = abs(a - b)

        if val > 4:
            if val == 5:
                return 3
            elif val == 6:
                return 2
            else:
                return 1
        else:
            return val

    def determine_yaw_between_cells(self, adj_x, adj_y, cell_x, cell_y):
        x1 = adj_x
        x2 = cell_x
        y1 = adj_y
        y2 = cell_y
        angle = math.degrees(math.atan2(x2 - x1, y2 - y1)) % 360

        if (0.0 <= angle and angle < 22.5) or (337.5 <= angle and angle <= 360):
            return 5
        elif 22.5 <= angle and angle < 67.5:
            return 4
        elif 67.5 <= angle and angle < 112.5:
            return 3
        elif 112.5 <= angle and angle < 157.5:
            return 2
        elif 157.5 <= angle and angle < 202.5:
            return 1
        elif 202.5 <= angle and angle < 247.5:
            return 8
        elif 247.5 <= angle and angle < 292.5:
            return 7
        elif 292.5 <= angle and angle < 337.5:
            return 6

    def solve(self):
        """Solve maze, find path to ending cell.

        @returns path or None if not found.
        """
        # add starting cell to open heap queue
        heapq.heappush(self.opened, (self.start.f, self.start,))
        while len(self.opened):
            # pop cell from heap queue
            f, cell = heapq.heappop(self.opened)
            #print("Focus on f={} cell= {}".format(f, cell))
            # add cell to closed list so we don't process it twice
            self.closed.add(cell)
            #print("Closed list: {}".format(self.closed))
            # if ending cell, return found path
            if cell is self.end:
                da_path = self.get_path()
                if da_path is not None:
                    return da_path
            # get adjacent cells for cell
            adj_cells = self.get_adjacent_cells(cell)
            for adj_cell in adj_cells:
                if adj_cell.reachable and adj_cell not in self.closed:
                    new_cost = self.get_cost(adj_cell, cell)    # Get path cost to this adjacent cell
                    if (adj_cell.f, adj_cell) in self.opened:
                        # if adj cell in open list, check if current path is
                        # better than the one previously found
                        # for this adj cell.
                        if adj_cell.g < new_cost:
                            # print("Updating cell... (%d, %d)" % (cell.x, cell.y))
                            self.update_cell(adj_cell, cell, cost=new_cost)
                    else:
                        self.update_cell(adj_cell, cell, cost=new_cost)
                        # add adj cell to open list
                        heapq.heappush(self.opened, (adj_cell.f, adj_cell))
                    # print("Looking at %d, %d, %d" % (adj_cell.x, adj_cell.y, adj_cell.z))
            #input("Press Enter to continue...")
        if len(self.hlist) == 0:
            return None
        else:
            new_end = (self.end.x, self.end.y, self.end.z, self.hlist.pop())
            # print("Searching for new goal: %s" % str(new_end))
            self.init_grid(self.grid_width, self.grid_height, walls, (self.start.x, self.start.y, self.start.z, self.start.yaw), new_end, self.grid_depth)
            return self.solve()

#-----------------------------------------------------------------------------------------------------------------------

# # Test script
# #
# print("A-Star Test")
# a = AStar2()
# walls = ((0, 0, 0), (0, 2, 0), (0, 3, 0), (0, 4, 0), (0, 6, 0), (0, 7, 0), (0, 8, 0), (0, 11, 0), (0, 13, 0), (0, 15, 0), (0, 16, 0), (0, 17, 0), (0, 18, 0), (0, 19, 0), (1, 2, 0), (1, 4, 0), (1, 13, 0), (1, 14, 0), (1, 15, 0), (1, 16, 0), (1, 17, 0), (1, 18, 0), (2, 6, 0), (3, 0, 0), (3, 1, 0), (3, 2, 0), (3, 3, 0), (3, 4, 0), (3, 8, 0), (3, 9, 0), (3, 10, 0), (3, 11, 0), (3, 12, 0), (3, 13, 0), (3, 14, 0), (3, 16, 0), (3, 18, 1), (3, 18, 0), (4, 0, 0), (4, 1, 0), (4, 2, 0), (4, 3, 0), (4, 4, 0), (4, 5, 0), (4, 6, 0), (4, 7, 0), (4, 8, 0), (4, 9, 1), (4, 9, 0), (4, 10, 1), (4, 10, 0), (4, 11, 0), (4, 13, 0), (4, 14, 0), (4, 18, 0), (4, 19, 0), (5, 0, 0), (5, 1, 0), (5, 2, 0), (5, 3, 0), (5, 4, 0), (5, 5, 1), (5, 5, 0), (5, 6, 0), (5, 7, 0), (5, 8, 0), (5, 9, 0), (5, 10, 0), (5, 11, 0), (5, 13, 0), (5, 14, 0), (5, 15, 0), (5, 16, 0), (5, 18, 0), (5, 19, 0), (6, 4, 1), (6, 4, 0), (6, 5, 2), (6, 5, 1), (6, 5, 0), (6, 7, 0), (6, 9, 0), (6, 13, 0), (6, 14, 0), (6, 15, 0), (6, 16, 0), (6, 18, 0), (6, 19, 0), (7, 3, 0), (7, 4, 1), (7, 4, 0), (7, 5, 2), (7, 5, 1), (7, 5, 0), (7, 6, 2), (7, 6, 1), (7, 6, 0), (7, 11, 0), (7, 12, 0), (7, 13, 0), (7, 14, 0), (7, 15, 0), (7, 16, 0), (7, 19, 0), (8, 0, 0), (8, 4, 1), (8, 4, 0), (8, 5, 2), (8, 5, 1), (8, 5, 0), (8, 6, 1), (8, 6, 0), (8, 7, 0), (8, 8, 0), (8, 13, 0), (8, 15, 0), (8, 16, 0), (8, 19, 0), (9, 0, 0), (9, 1, 0), (9, 2, 0), (9, 4, 0), (9, 5, 2), (9, 5, 1), (9, 5, 0), (9, 6, 1), (9, 6, 0), (9, 7, 0), (9, 8, 0), (9, 15, 0), (10, 3, 0), (10, 5, 1), (10, 5, 0), (10, 6, 2), (10, 6, 1), (10, 6, 0), (10, 7, 1), (10, 7, 0), (10, 8, 0), (10, 13, 0), (10, 15, 0), (11, 1, 0), (11, 3, 0), (11, 4, 0), (11, 5, 0), (11, 6, 0), (11, 7, 2), (11, 7, 1), (11, 7, 0), (11, 8, 2), (11, 8, 1), (11, 8, 0), (11, 9, 1), (11, 9, 0), (11, 10, 0), (11, 11, 0), (11, 14, 1), (11, 14, 0), (11, 15, 1), (11, 15, 0), (12, 1, 0), (12, 3, 0), (12, 4, 0), (12, 6, 0), (12, 7, 1), (12, 7, 0), (12, 8, 0), (12, 9, 0), (12, 10, 0), (12, 11, 1), (12, 11, 0), (12, 12, 0), (12, 13, 1), (12, 13, 0), (12, 14, 3), (12, 14, 2), (12, 14, 1), (12, 14, 0), (12, 15, 2), (12, 15, 1), (12, 15, 0), (13, 0, 0), (13, 1, 0), (13, 3, 0), (13, 4, 0), (13, 6, 0), (13, 7, 0), (13, 10, 0), (13, 11, 2), (13, 11, 1), (13, 11, 0), (13, 12, 2), (13, 12, 1), (13, 12, 0), (13, 13, 3), (13, 13, 2), (13, 13, 1), (13, 13, 0), (13, 14, 2), (13, 14, 1), (13, 14, 0), (13, 15, 1), (13, 15, 0), (14, 3, 0), (14, 6, 0), (14, 7, 0), (14, 8, 0), (14, 9, 0), (14, 11, 0), (14, 12, 1), (14, 12, 0), (14, 13, 0), (14, 14, 0), (14, 15, 0), (14, 16, 0), (15, 0, 0), (15, 2, 0), (15, 3, 0), (15, 4, 0), (15, 5, 0), (15, 7, 0), (15, 11, 0), (15, 12, 0), (15, 13, 0), (15, 15, 0), (15, 17, 0), (16, 1, 0), (16, 2, 1), (16, 2, 0), (16, 3, 0), (16, 4, 0), (16, 5, 1), (16, 5, 0), (16, 9, 0), (16, 10, 0), (16, 11, 0), (17, 0, 0), (17, 1, 0), (17, 2, 2), (17, 2, 1), (17, 2, 0), (17, 3, 1), (17, 3, 0), (17, 4, 0), (17, 5, 1), (17, 5, 0), (17, 6, 1), (17, 6, 0), (17, 7, 0), (17, 9, 0), (17, 10, 0), (17, 11, 0), (17, 12, 0), (17, 13, 0), (17, 14, 0), (17, 15, 0), (17, 16, 0), (17, 18, 0), (18, 1, 1), (18, 1, 0), (18, 2, 3), (18, 2, 2), (18, 2, 1), (18, 2, 0), (18, 3, 1), (18, 3, 0), (18, 4, 0), (18, 5, 1), (18, 5, 0), (18, 6, 2), (18, 6, 1), (18, 6, 0), (18, 7, 0), (18, 9, 0), (18, 10, 0), (18, 11, 0), (18, 12, 0), (18, 14, 0), (18, 15, 0), (18, 16, 0), (18, 19, 1), (18, 19, 0), (19, 0, 0), (19, 1, 1), (19, 1, 0), (19, 2, 2), (19, 2, 1), (19, 2, 0), (19, 3, 0), (19, 4, 0), (19, 5, 0), (19, 6, 2), (19, 6, 1), (19, 6, 0), (19, 7, 1), (19, 7, 0), (19, 8, 0), (19, 9, 0), (19, 10, 0), (19, 11, 0), (19, 12, 0), (19, 16, 0))
# a.init_grid(20, 20, walls, (1, 5, 0, 3), (16, 13, 0, 5), 3)
# path = a.solve()
# print(path)
#
# # Map generation...
# #
#
# new_walls = []
# for (x, y, z) in walls:
#     new_walls.append((x, y, z+1))
#
# display_walls = []
# for i in range(20):
#     for j in range(20):
#         set = []
#         for (x, y, z) in new_walls:
#             if x == i and y == j:
#                 set.append((x, y, z))
#         if len(set) > 0:
#             set.sort(key=lambda k: k[2])
#             val = set[len(set)-1]
#             display_walls.append(val)
#             # print("%s => %s" % (str(set), str(val)))
#
# #print(display_walls)
#
# new_path = []
# if path is not None:
#     for (x,y,z,h) in path:
#         new_path.append((x,y,z+1,h))
#
# map_gen = MapGenerator2(map=display_walls)
# map_gen.create_file(filename="mission/search.png", path=new_path, special={})