from copy import deepcopy
from setuptools import setup, find_packages, Extension
from Cython.Build import cythonize
from Cython.Compiler import Options
from Cython.Distutils import build_ext

Options.docstrings = False

source_files = ['./astar_pathfinding/search.pyx', './astar_pathfinding/astar.pyx']
kwargs = {'include_dirs': ['astar_pathfinding']}

extensions = [
    Extension('search', source_files, language='c++', **deepcopy(kwargs))
]

setup(
    name='astar_pathfinding',
    version='1.0.3',
    packages=find_packages(exclude=['tests']),

    url='https://gitlab.com/cogle-public/astar_pathfinding',
    author='Jacob Le',
    author_email='jacob.le@parc.com',
    description='A* pathfinding algorithms used in COGLE projects',
    
    ext_modules = cythonize('./astar_pathfinding/*.pyx', compiler_directives={'language_level': 3}), # cythonize(extensions),
    long_description=open('README.md').read(),
    zip_safe=False,
)