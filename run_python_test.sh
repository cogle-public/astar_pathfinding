#!/bin/bash

# Performs a unit test of the built local files from a python file

activate () {
    source tests/LIBENV/bin/activate
    python3 tests/python_test.py
}

activate